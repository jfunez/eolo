# Eolo.

## Description:

This is a website for uploading weather information. Allows to view, edit and add (manually and via restfull API)
The base idea is to group weather information aquired by unattended weather stations, who connect by an API, then this website shows in a user friendly way.
Also the user can edit 

## To Do:

Comming soon:
*   data-edit restrictions: users (or admins) only can edit self data
*   tests a python script to upload data via API. Not only download. Requiring an API Key feature.
