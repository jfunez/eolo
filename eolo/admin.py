# -*- coding: utf-8 -*-
from django.contrib import admin
from eolo.models import Station, Node, Sensor, Measure


class StationAdmin(admin.ModelAdmin):
    pass


class NodeAdmin(admin.ModelAdmin):
    pass


class SensorAdmin(admin.ModelAdmin):
    pass


class MeasureAdmin(admin.ModelAdmin):
    pass


admin.site.register(Station, StationAdmin)
admin.site.register(Node, NodeAdmin)
admin.site.register(Sensor, SensorAdmin)
admin.site.register(Measure, MeasureAdmin)
