# -*- coding: utf-8 -*-
from django.forms import ModelForm
from eolo.models import Station, Node, Sensor, Measure


class StationForm(ModelForm):
    class Meta:
        model = Station


class NodeForm(ModelForm):
    class Meta:
        model = Node


class SensorForm(ModelForm):
    class Meta:
        model = Sensor


class MeasureForm(ModelForm):
    class Meta:
        model = Measure
