# -*- coding: utf-8 -*-
from tastypie.resources import ModelResource
from tastypie.authorization import Authorization

from eolo.models import Station, Node, Sensor, Measure


class StationResource(ModelResource):
    class Meta:
        queryset = Station.objects.all()
        resource_name = 'station'
        allowed_methods = ['get', 'delete', 'post', 'put']
        authorization = Authorization()


class NodeResource(ModelResource):
    class Meta:
        queryset = Node.objects.all()
        resource_name = 'node'
        allowed_methods = ['get', 'delete', 'post', 'put']
        authorization = Authorization()


class SensorResource(ModelResource):
    class Meta:
        queryset = Sensor.objects.all()
        resource_name = 'sensor'
        allowed_methods = ['get', 'delete', 'post', 'put']
        authorization = Authorization()


class MeasureResource(ModelResource):
    class Meta:
        queryset = Measure.objects.all()
        resource_name = 'measure'
        allowed_methods = ['get', 'delete', 'post', 'put']
        authorization = Authorization()
