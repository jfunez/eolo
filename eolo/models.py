# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _


class Station(models.Model):
    name = models.CharField(max_length=20, default="")
    mantainer = models.ForeignKey(User, related_name='mantainer')

    class Meta:
        verbose_name = _('Station')
        verbose_name_plural = _('Stations')
        ordering = ['name', ]

    def __unicode__(self):
        return self.name


class Node(models.Model):
    name = models.CharField(max_length=20, default="")
    station = models.ForeignKey(Station, related_name='nodes')
    latitude = models.CharField(max_length=100, default="")
    longitude = models.CharField(max_length=100, default="")
    elevation = models.PositiveIntegerField(default=0)

    @property
    def coord(self):
        return "%s, %s" % (self.latitude, self.longitude)

    class Meta:
        verbose_name = _('Node')
        verbose_name_plural = _('Nodes')
        ordering = ['station', 'name', ]

    def __unicode__(self):
        return self.name


SENSOR_MEASURE_TYPE = (
    ('0', _('Thermometer')),
    ('1', _('Anemometer')),
    ('2', _('Barometer')),
    ('3', _('Hygrometer')),
    ('4', _('Pluviometer')),
)
SENSOR_SIGNAL_TYPE = (
    ('0', _('Analog')),
    ('1', _('Digital')),
)


class Sensor(models.Model):
    brand = models.CharField(max_length=80, default="", blank=True, null=True)
    model = models.CharField(max_length=80, default="", blank=True, null=True)
    node = models.ForeignKey(Node, related_name='sensors')
    measure_type = models.CharField(max_length=80, default="0", choices=SENSOR_MEASURE_TYPE)
    signal_type = models.CharField(max_length=80, default="0", choices=SENSOR_SIGNAL_TYPE)

    class Meta:
        verbose_name = _('Sensor')
        verbose_name_plural = _('Sensors')
        ordering = ['node', 'measure_type', 'signal_type', ]

    def __unicode__(self):
        return "[%s] %s - %s" % (self.node, self.get_measure_type_display(), self.get_signal_type_display())


MEASURE_UNIT_CHOICES = (
    ('0', _('percent')),
    ('1', _('celsius')),
    ('2', _('farenheit')),
    ('3', _('meter')),
    ('4', _('kilometer')),
    ('5', _('hPa')),
)


class Measure(models.Model):
    sensor = models.ForeignKey(Sensor, related_name='measures')
    timestamp = models.DateTimeField(auto_now=True)
    value = models.CharField(max_length=20, default="")
    unit = models.CharField(max_length=10, default="", choices=MEASURE_UNIT_CHOICES)

    class Meta:
        verbose_name = _('Measure')
        verbose_name_plural = _('Measures')
        ordering = ['timestamp', ]

    def __unicode__(self):
        return "%s: %s %s" % (self.sensor, self.value, self.get_unit_display())
