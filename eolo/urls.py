from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from django.views.generic import ListView, DetailView, UpdateView
from eolo.models import Station, Node, Sensor, Measure
from eolo.forms import StationForm, NodeForm, SensorForm, MeasureForm
from eolo.views import AddStationView, AddNodeView, AddSensorView, AddMeasureView
from tastypie.api import Api
from eolo.api import StationResource, NodeResource, SensorResource, MeasureResource

admin.autodiscover()
v1_api = Api(api_name='v1')
v1_api.register(StationResource())
v1_api.register(NodeResource())
v1_api.register(SensorResource())
v1_api.register(MeasureResource())


def get_obj_by_name(name):
    result_data = {
        'name': name,
        'paginate_by': 15,
        'detail_context_object_name': name,
        'list_context_object_name': "%ss" % name,
    }
    if name == 'station':
        result_data['model'] = Station
        result_data['queryset'] = Station.objects.all()
        result_data['add_view'] = AddStationView
        result_data['edit'] = StationForm
    elif name == 'node':
        result_data['model'] = Node
        result_data['queryset'] = Node.objects.all()
        result_data['add_view'] = AddNodeView
        result_data['edit'] = NodeForm
    elif name == 'sensor':
        result_data['model'] = Sensor
        result_data['queryset'] = Sensor.objects.all()
        result_data['add_view'] = AddSensorView
        result_data['edit'] = SensorForm
    elif name == 'measure':
        result_data['model'] = Measure
        result_data['queryset'] = Measure.objects.all()
        result_data['add_view'] = AddMeasureView
        result_data['edit'] = MeasureForm
    elif name == 'user':
        result_data['model'] = User
        result_data['queryset'] = User.objects.all()
        result_data['detail_context_object_name'] = 'usr'  # user and users already taken by login user
        result_data['list_context_object_name'] = 'usrs'  # user and users already taken by login user
        result_data['add_view'] = None
        result_data['edit'] = None
    return result_data

urlpatterns = patterns('',
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': 'accounts/login.html'}, name="login"),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'template_name': 'accounts/logged_out.html'}, name='logout'),
    url(r'^$', 'eolo.views.home', name='home'),
    # Admin
    url(r'^admin/', include(admin.site.urls)),
    # API
    url(r'^api/', include(v1_api.urls)),
)

for obj_name in ('station', 'node', 'sensor', 'measure', 'user'):
    add_view = get_obj_by_name(obj_name)['add_view']
    edit = get_obj_by_name(obj_name)['add_view']
    if add_view:
        urlpatterns += patterns('',
            # -> add
            url(r'^%s/add/$' % obj_name, login_required(add_view.as_view()), name="%s_add" % obj_name,),
        )

    if edit:
        urlpatterns += patterns('',
            url('^%s/edit/(?P<pk>[\w-]+)/$' % obj_name, login_required(UpdateView.as_view(
                            model=get_obj_by_name(obj_name)['model'],
                            form_class=get_obj_by_name(obj_name)['edit'],
                            template_name='eolo/%s/edit.html' % obj_name,
                            success_url='/%s/' % obj_name,
                        )), name="%s_edit" % obj_name,),
        )

    urlpatterns += patterns('',
        # -> detail
        url(r'^%s/(?P<pk>\d+)/$' % obj_name,
            login_required(DetailView.as_view(
                            context_object_name=get_obj_by_name(obj_name)['detail_context_object_name'],
                            model=get_obj_by_name(obj_name)['model'],
                            template_name='eolo/%s/detail.html' % obj_name,
                        )),
            name=obj_name
        ),
        # -> list
        url(r'^%s/$' % obj_name,
            login_required(ListView.as_view(
                            model=get_obj_by_name(obj_name)['model'],
                            paginate_by=get_obj_by_name(obj_name)['paginate_by'],
                            queryset=get_obj_by_name(obj_name)['queryset'],
                            context_object_name=get_obj_by_name(obj_name)['list_context_object_name'],
                            template_name='eolo/%s/list.html' % obj_name,
                        )),
            name="%s_list" % obj_name,
        ),
    )

if settings.DEBUG:
    urlpatterns += patterns('django.views.static',
        url(r'^static/(?P<path>.*)$', 'serve', {'document_root': settings.STATIC_ROOT}),
        url(r'^media/(?P<path>.*)$',  'serve', {'document_root': settings.MEDIA_ROOT}),
    )
