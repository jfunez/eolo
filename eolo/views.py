# -*- coding: utf-8 -*-
from django.template import Context, RequestContext
from django.shortcuts import render_to_response
from django.views.generic.edit import FormView
from eolo.forms import StationForm, NodeForm, SensorForm, MeasureForm


def home(request):
    context = Context({
    })
    return render_to_response('home.html', context, context_instance=RequestContext(request))


class AddStationView(FormView):
    form_class = StationForm
    success_url = "/station/"
    template_name = "eolo/station/add.html"

    def form_valid(self, form):
        form.save(commit=True)
        return super(AddStationView, self).form_valid(form)


class AddNodeView(FormView):
    form_class = NodeForm
    success_url = "/node/"
    template_name = "eolo/node/add.html"

    def form_valid(self, form):
        form.save(commit=True)
        return super(AddNodeView, self).form_valid(form)


class AddSensorView(FormView):
    form_class = SensorForm
    success_url = "/sensor/"
    template_name = "eolo/sensor/add.html"

    def form_valid(self, form):
        form.save(commit=True)
        return super(AddSensorView, self).form_valid(form)


class AddMeasureView(FormView):
    form_class = MeasureForm
    success_url = "/measure/"
    template_name = "eolo/measure/add.html"

    def form_valid(self, form):
        form.save(commit=True)
        return super(AddMeasureView, self).form_valid(form)
